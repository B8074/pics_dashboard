import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash
from dash.dependencies import Output, Input, State, ALL
import plotly.graph_objs as go
import dash_auth
import pickle

# Initializing app
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

# Username and Password authentication
# [[username, password]]
auth = dash_auth.BasicAuth(
    app,
    [['steve', 'young']]
)

# Common_page

# Uploading information for residential building page from Residential_buildings pickle file
residential_infile = open("sources_pickle/Residential_Buildings", 'rb')
residential_results = pickle.load(residential_infile)
residential_infile.close()

# Common_page
# Uploading information for transportation page from Transportation pickle file
transportation_infile = open("sources_pickle/Transportation", 'rb')
transportation_results = pickle.load(transportation_infile)
transportation_infile.close()

# Uploading information for home page from Summary pickle file
summary_infile = open("sources_pickle/Summary", 'rb')
summary_results = pickle.load(summary_infile)
summary_infile.close()

# Uploading information for transit route page from Transit_Routes pickle file
transit_infile = open("sources_pickle/Transit_Routes", 'rb')
transit_results = pickle.load(transit_infile)
transit_infile.close()

# Uploading information for transit route page dropdown menu from Transit_Routes_Labels pickle file
routes_infile = open("sources_pickle/Transit_Route_Labels", 'rb')
victoria_transit_routes = pickle.load(routes_infile)
routes_infile.close()


def get_group_numbers(results):
    """A function that fetches all the group numbers in the specified pickle file data.

            :param results: The Pickle file data

            :returns: A list containing all the group numbers.
    """
    group_numbers_list = []
    for i in range(len(results[1]['group'])):
        if results[1]['group'].iloc[i] not in group_numbers_list:
            group_numbers_list.append(results[1]['group'].iloc[i])
    return group_numbers_list


def get_group_number_indexes(results, group_number):
    """A function that fetches the indexes of all the policies in the specified group within the stated pickle file data.

        :param results: The Pickle file data
        :param group_number: The specified group number to which policies are seperated into different menus

        :returns: A list containing the indexes of all the policies in the specified group.
    """
    index_list = []
    for i in range(len(results[1]['group'])):
        if results[1]['group'].iloc[i] == group_number:
            index_list.append(i)
    return index_list


def get_default_policies(results, group_number):
    """A function that fetches the indexes of all the policies that are to be enabled by default for a specific group.

        :param results: The Pickle file data
        :param group_number: The Group of policies to be analyzed

        :returns: A list or a single column index of the policies that are to be enabled by default.
    """
    default_policies = []
    for i in range(len(results[1]['enabled'])):
        if results[1]['group'].iloc[i] == group_number:
            if results[1]['selection-type'].iloc[i] == 'OR':
                if results[1]['enabled'].iloc[i]:
                    return i
            elif results[1]['selection-type'].iloc[i] == 'DROPDOWN':
                if results[1]['enabled'].iloc[i]:
                    return i
            else:
                if results[1]['enabled'].iloc[i]:
                    default_policies.append(i)
    return default_policies


def get_sub_sectors(results):
    """A function that fetches all the sub-sectors from the specified pickle file data and creates a list of dropdown menu items with an href to the sub-secotr pages.

        :param results: The Pickle file data

        :returns: A list containing the dropdown menu items for every sub-sector contained in results with their accompanying href.
    """
    dropdown_menu_item_list = []
    for i in range(len(results[1]['sub-sector-menu'].iloc[1])):
        dropdown_menu_item_list.append(dbc.DropdownMenuItem(
                    results[1]['sub-sector-menu'].iloc[1][i],
                    href=results[1]['sub-sector-menu'].iloc[2][i],
                ))
    return dropdown_menu_item_list


# The layout of the home page of the Dashboard
home_page = html.Div(id='home_page', children=[
    html.H2(children='Summary of GHG Emissions in Victoria, BC', className='page-title'),
    dcc.Graph(id='summary-graph', className='graph'),
    html.H3(children='Policies', className='page-component'),
    html.Br(),
    html.Div(id='policy-summary', className='page-component'),
    html.Br(),
    html.H3(children='Implementations', className='page-component'),
    html.Br(),
    html.Div(id='implementation-summary', className='page-component'),
    html.Button(id='never-used', style=dict(display='none'))
])
# Common_page

# The layout of the building page of the Dashboard
building_page = html.Div(id='building_page', children=[
    html.H2(children=residential_results[0], className='page-title'),
    dcc.Graph(id='residential-graph', className='graph'),
    html.H3(children='Policies', className='page-component'),
    html.Div(id='building_menus', children=[], className='page-component'),
    html.Br(),
    html.H3(children='Implementations', className='page-component'),
    html.Br(),
    html.Div(id='residential-implementation-container-output', className='page-component'),
    dbc.Button('Jupyter Notebook Reference', className='notebook-button', href="/building-notebook-page"),
])
# Common_page

# The layout of the transportation page of the Dashboard
transportation_page = html.Div(id='transportation_page', children=[
    html.H2(children=transportation_results[0], className='page-title'),
    dcc.Graph(id='electrification-graph', className='graph'),
    html.Div(id='transportation-sub-sectors', className='page-component'),
    html.H3(children='Electrification of Victoria transit fleet', className='page-component'),
    html.Div(id='transportation_menus', children=[], className='page-component'),
    html.Br(),
    html.H3(children='Implementations', className='page-component'),
    html.Br(),
    html.Div(id='transportation-implementation-container-output', className='page-component'),
    dbc.Button('Transit Routes', className='transportation-button', href="/transit-route-page"),
    html.Br(),
    # link to the notebooks. Could be a replacement in the datafile.
    dbc.Button('Jupyter Notebook Reference', className='notebook-button', href="/transportation-notebook-page"),
])

# The page containing the buildings jupyter notebook
building_notebook_page = html.Div(id='building_notebook_page', children=[
    html.Iframe(src="/static/2020FuelConsumption-post.html", className='notebook')
])

# The page containing the buildings jupyter notebook
transportation_notebook_page = html.Div(id='transportation_notebook_page', children=[
    html.Iframe(src="/static/Transportation Output.html", className='notebook')
])

# The layout of the transit route page of the Dashboard
transit_route_page = html.Div(id='transit_route_page', children=[
    html.H2(children=transit_results[0], className='page-title'),
    dcc.Graph(id='transit-graph', className='graph'),
    html.Div(id='transit_menus', children=[], className='page-component'),
    html.H4(children='Policies', className='page-component'),
    html.Br(),
    html.P(children=transit_results[1]['input-label-1'].iloc[0], className='page-component'),
    dcc.Input(id="input-1", type=transit_results[1]['input-1-type'].iloc[0], min=0, className='input'),
    html.Br(),
    html.P(children=transit_results[1]['input-label-2'].iloc[0], className='page-component'),
    dcc.Input(id="input-2", type=transit_results[1]['input-2-type'].iloc[0], min=0, className='input'),
    dcc.Slider(
        id='transit-slider',
        className='transit-slider',
        min=transit_results[1]['years'].iloc[0],
        max=transit_results[1]['years'].iloc[2],
        step=1,
        value=transit_results[1]['years'].iloc[1],
    ),
    html.Div(id='transit-slider-output-container', className='transit-slider-output')
])

# The navigation bar implemented in the dashboard
# Includes a dropdown menu which contains the radioitems where the user can choose the population growth
# Also includes a button where the user can toggle the visibility of the Strategies Sidebar
navbar = dbc.NavbarSimple(
    children=[
        dbc.DropdownMenu(
            children=[
                dbc.RadioItems(
                    id="scaling-radio",
                    options=[{'label': 'Low Growth (0.5%/year) ', 'value': 1.005},
                             {'label': 'Medium Growth (0.75%/year)', 'value': 1.0075},
                             {'label': 'High Growth (1.25%/year)', 'value': 1.0125}],
                    value=1.005
                ),
            ],
            id='scaling-dropdown',
            style={'position': 'static'},  # Only included so that dropdown is visible, remove when issue is resolved.
            label="Population Growth"
        ),
        dbc.Button("Toggle Navigation Sidebar", color="primary", id="btn_sidebar"),

    ],
    fixed='top',
    brand_href="#",
    brand='PICS GHG Emissions Dashboard',
    color="dark",
    dark=True,
    fluid=True,
)

# The sidebar implemented in the Dashboard
# Includes buttons to navigate to the summary, transportation and residential buildings pages of the dashboard

# This should only structure common-pages
sidebar = html.Div([
    dbc.Button(summary_results[0], className='sidebar-button', href="/home-page"),
    dbc.Button(transportation_results[0], className='sidebar-button', href="/transportation-page"),
    dbc.Button(residential_results[0], className='sidebar-button', href="/building-page"),
],
    id="sidebar",
    className='sidebar-visible',
)

# General layout of each page containing a url and a unique page layout
app.layout = html.Div([
    dcc.Store(id='side_click'),
    dcc.Location(id='url', refresh=False),
    html.Div(id='layout-content', children=[navbar, sidebar, home_page, transportation_page, building_page,
                                            building_notebook_page, transportation_notebook_page, transit_route_page])
])


# Callback which detects if the button toggling the visibility of the sidebar has been pressed by the user. If so, the
# the callback will look at the state of side_click and updated both the style of the sidebar and every page by changing
# the className value. side_click will also be updated to reflect the change in state.
@app.callback([Output("sidebar", "className"), Output("layout-content", "className"),
               Output("side_click", "data")],
              [Input("btn_sidebar", "n_clicks")],
              [State("side_click", "data")])
def toggle_sidebar(n, n_click):
    """A function that changes the style and visibility of the sidebar depending on the state of n_click.

        :param n: The number of times that the toggle navigation sidebar button has been clicked.
        :param n_click: A string that states whether the sidebar is to be hidden or shown on the next click of the toggle sidebar button.

        :returns: Changes to the style of both the sidebar and the rest of the page with the sidebar changing from being shown to hidden and vice versa.
    """
    if n:
        if n_click == "SHOW":
            sidebar_style = 'sidebar-hidden',
            content_style = 'content-without-sidebar',
            cur_n_click = "HIDDEN",
        else:
            sidebar_style = 'sidebar-visible'
            content_style = 'content-with-sidebar'
            cur_n_click = "SHOW"
    else:
        sidebar_style = 'sidebar-visible'
        content_style = 'content-with-sidebar'
        cur_n_click = 'SHOW'

    return sidebar_style, content_style, cur_n_click


# Callback that takes a pathname as an input and sets the page with the pathname as visible while setting all the height
# of every other page in the list to 0 and thus making them invisible.
@app.callback(
    [Output(page, 'style') for page in ['home_page', 'transportation_page', 'building_page', 'building_notebook_page',
                                        'transportation_notebook_page', 'transit_route_page']],
    [Input('url', 'pathname')])
def display_page(pathname):
    """A function that sets the visibility of each page of the dashboard depending on the current url.

            :param pathname: The url of the current page.

            :returns: The styles of each page of the dashboard with all pages set to being invisible apart from the page with the current url.
    """
    return_value = [{'line-height': '0', 'height': '0', 'overflow': 'hidden'} for _ in range(6)]
    if pathname == '/transportation-page':
        return_value[1] = {'height': 'auto'}
        return return_value
    elif pathname == '/building-page':
        return_value[2] = {'height': 'auto'}
        return return_value
    elif pathname == '/building-notebook-page':
        return_value[3] = {'height': 'auto'}
        return return_value
    elif pathname == '/transportation-notebook-page':
        return_value[4] = {'height': 'auto'}
        return return_value
    elif pathname == '/transit-route-page':
        return_value[5] = {'height': 'auto'}
        return return_value
    else:
        return_value[0] = {'height': 'auto'}
        return return_value


@app.callback(
    [Output('transportation_menus', 'children'), Output('building_menus', 'children'),
     Output('transit_menus', 'children')],
    Input('never-used', 'n_clicks'),
    [State('transportation_menus', 'children'), State('building_menus', 'children'),
     State('transit_menus', 'children')])
def display_menus(n_clicks, transportation_children, building_children, transit_children):
    """A pattern-matching callback that creates checklist and radioitems as specified by each page's pickle file.

            :param n_clicks: Random input that is never used.
            :param transportation_children: A list where all the created transportation menus will be entered in.
            :param building_children: A list where all the created building menus will be entered in.
            :param transit_children: A list where all the created transit route menus will be entered in.

            :returns: All the menus for each page as specified by their pickle files.
        """
    transportation_list = get_group_numbers(transportation_results)
    for i in range(len(transportation_list)):
        transportation_index_list = get_group_number_indexes(transportation_results, transportation_list[i])
        if transportation_results[1]['selection-type'].iloc[transportation_index_list[0]] == 'AND':
            new_menu = dcc.Checklist(
                id={
                    'type': 'transportation-menu',
                    'index': i
                },
                options=[{'label': transportation_results[1]['label'].iloc[j], 'value': j} for j in
                         transportation_index_list],
                labelStyle={'display': 'block'},
                value=get_default_policies(transportation_results,
                                           transportation_results[1]['group'].iloc[transportation_index_list[i]])
            )
            transportation_children.append(new_menu)
        elif transportation_results[1]['selection-type'].iloc[transportation_index_list[0]] == 'OR':
            new_menu = dcc.RadioItems(
                id={
                    'type': 'transportation-menu',
                    'index': i
                },
                options=[{'label': transportation_results[1]['label'].iloc[j], 'value': j} for j in
                         transportation_index_list],
                labelStyle={'display': 'block'},
                value=get_default_policies(transportation_results,
                                           transportation_results[1]['group'].iloc[transportation_index_list[i]]),
            )
            transportation_children.append(new_menu),

    building_list = get_group_numbers(residential_results)
    for i in range(len(building_list)):
        building_index_list = get_group_number_indexes(residential_results, building_list[i])
        if residential_results[1]['selection-type'].iloc[building_index_list[0]] == 'AND':
            new_menu = dcc.Checklist(
                id={
                    'type': 'building-menu',
                    'index': i
                },
                options=[{'label': residential_results[1]['label'].iloc[j], 'value': j} for j in building_index_list],
                labelStyle={'display': 'block'},
                value=get_default_policies(residential_results,
                                           residential_results[1]['group'].iloc[building_index_list[i]]),
            )
            building_children.append(new_menu)
        elif residential_results[1]['selection-type'].iloc[building_index_list[0]] == 'OR':
            new_menu = dcc.RadioItems(
                id={
                    'type': 'building-menu',
                    'index': i
                },
                options=[{'label': residential_results[1]['label'].iloc[j], 'value': j} for j in building_index_list],
                labelStyle={'display': 'block'},
                value=get_default_policies(residential_results,
                                           residential_results[1]['group'].iloc[building_index_list[i]]),
            )
            building_children.append(new_menu)

    transit_list = get_group_numbers(victoria_transit_routes)
    for i in range(len(transit_list)):
        transit_index_list = get_group_number_indexes(victoria_transit_routes, transit_list[i])
        if victoria_transit_routes[1]['selection-type'].iloc[transit_index_list[0]] == 'AND':
            new_menu = dcc.Checklist(
                id={
                    'type': 'transit-menu',
                    'index': i
                },
                options=[{'label': victoria_transit_routes[1]['label'].iloc[j], 'value': j} for j in
                         transit_index_list],
                labelStyle={'display': 'block'},
                value=get_default_policies(victoria_transit_routes,
                                           victoria_transit_routes[1]['group'].iloc[transit_index_list[i]]),
            )
            transit_children.append(new_menu)
        elif victoria_transit_routes[1]['selection-type'].iloc[transit_index_list[0]] == 'OR':
            new_menu = dcc.RadioItems(
                id={
                    'type': 'transit-menu',
                    'index': i
                },
                options=[{'label': victoria_transit_routes[1]['label'].iloc[j], 'value': j} for j in
                         transit_index_list],
                labelStyle={'display': 'block'},
                value=get_default_policies(victoria_transit_routes,
                                           victoria_transit_routes[1]['group'].iloc[transit_index_list[i]]),
            )
            transit_children.append(new_menu)
        elif victoria_transit_routes[1]['selection-type'].iloc[transit_index_list[0]] == 'DROPDOWN':
            new_menu = dcc.Dropdown(
                id={
                    'type': 'transit-menu',
                    'index': i
                },
                options=[{'label': victoria_transit_routes[1]['route-label'].iloc[j], 'value': j} for j in
                         transit_index_list],
                placeholder=victoria_transit_routes[1]['placeholder'].iloc[0],
                className='dropdown',
                value=get_default_policies(victoria_transit_routes,
                                           victoria_transit_routes[1]['group'].iloc[transit_index_list[i]]),
            )
            transit_children.append(new_menu)
    return transportation_children, building_children, transit_children


@app.callback([Output("transportation-implementation-container-output", "children"),
               Output("residential-implementation-container-output", "children"),
               Output("implementation-summary", "children"), Output("policy-summary", "children")],
              [Input({'type': 'transportation-menu', 'index': ALL}, 'value'),
               Input({'type': 'building-menu', 'index': ALL}, 'value')])
def display_implementation_and_policy(transportation_index, building_index):
    """A pattern-matching callback that displays the implementations for the summary, transportation, and building pages.

        :param transportation_index: A list containing the inputs from each transportation menu.
        :param building_index: A list containing the inputs from each transportation menu.

        :returns: Two lists containing the implementations for the transportation and building pages as well as two dcc.Markdown lists with the policies and implementations for the summary page.
    """
    total_implementations = []
    total_policies = []
    transportation_implementations = []
    for i in range(len(transportation_index)):
        transportation_menu_implementations = []
        if transportation_index[i] is None:
            pass
        else:
            if not isinstance(transportation_index[i], list):
                transportation_index[i] = [transportation_index[i]]
            for j in range(len(transportation_index[i])):
                transportation_menu_implementations.append(
                    transportation_results[1]['implementation'].iloc[transportation_index[i][j]])
                total_implementations.append(
                    transportation_results[1]['implementation'].iloc[transportation_index[i][j]])
                total_policies.append(transportation_results[1]['label'].iloc[transportation_index[i][j]])
            transportation_implementations.append(html.Ul([html.Li(x) for x in transportation_menu_implementations]))
    residential_implementations = []
    for i in range(len(building_index)):
        residential_menu_implementations = []
        if building_index[i] is None:
            pass
        else:
            if not isinstance(building_index[i], list):
                building_index[i] = [building_index[i]]
            for j in range(len(building_index[i])):
                residential_menu_implementations.append(
                    residential_results[1]['implementation'].iloc[building_index[i][j]])
            residential_implementations.append(html.Ul([html.Li(x) for x in residential_menu_implementations]))
            # Temporary code, to be replaced
            if len(building_index[i]) != 0:
                if residential_results[1]['implementation'].iloc[1] in total_implementations:
                    pass
                else:
                    total_implementations.append(residential_results[1]['implementation'].iloc[0])
                    total_policies.append(residential_results[1]['label'].iloc[0])

    return transportation_implementations, residential_implementations, html.Ul(
        [html.Li(x) for x in total_implementations]), \
           html.Ul([html.Li(x) for x in total_policies])


@app.callback([Output("electrification-graph", "figure"), Output("residential-graph", "figure"),
               Output("summary-graph", "figure")],
              [Input({'type': 'transportation-menu', 'index': ALL}, 'value'),
               Input({'type': 'building-menu', 'index': ALL}, 'value')])
def display_graph(transportation_index, building_index):
    """A pattern-matching callback that displays the graphs for the summary, transportation, and building pages.

        :param transportation_index: A list containing the inputs from each transportation menu.
        :param building_index: A list containing the inputs from each transportation menu.

        :returns: Three plotly_graph_objs graphs with GHG emissions.
    """
    summary_fig = go.Figure()
    transportation_fig = go.Figure()

    for i in range(len(transportation_index)):
        if transportation_index[i] is None:
            pass
        else:
            if not isinstance(transportation_index[i], list):
                transportation_index[i] = [transportation_index[i]]
            if len(transportation_index[i]) != 0:
                summary_fig.add_trace(transportation_results[1]['summary-graph-type'].iloc[transportation_index[i][0]]
                                      (**transportation_results[1]['long-distance-first'].iloc[
                                          transportation_index[i][0]],
                                       name=summary_results[1]['transit-trace-name'].iloc[0]))
            for j in range(len(transportation_index[i])):
                transportation_fig.add_trace(transportation_results[1]['graph-type'].iloc[transportation_index[i][j]]
                                             (**transportation_results[1]['long-distance-first'].iloc[
                                                 transportation_index[i][j]],
                                              name=transportation_results[1]['long-distance-first-name'].iloc[
                                                  transportation_index[i][j]]))
                transportation_fig.add_trace(transportation_results[1]['graph-type'].iloc[transportation_index[i][j]]
                                             (**transportation_results[1]['short-distance-first'].iloc[
                                                 transportation_index[i][j]],
                                              name=transportation_results[1]['short-distance-first-name'].iloc[
                                                  transportation_index[i][j]]))
            transportation_fig.update_layout(
                title=transportation_results[1]['graph-title'].iloc[0],
                xaxis_title=transportation_results[1]['x-label'].iloc[0],
                yaxis_title=transportation_results[1]['y-label'].iloc[0],
                xaxis_tickformat='%Y',
                xaxis=dict(
                    rangeslider=dict(
                        visible=True
                    ),
                    type="date"
                ),
            )

    building_fig = go.Figure()
    for i in range(len(building_index)):
        if building_index[i] is None:
            pass
        else:
            if not isinstance(building_index[i], list):
                building_index[i] = [building_index[i]]
            # Temporary Code, will be replaced when summary building data available
            if len(building_index[i]) != 0:
                summary_fig.add_trace(residential_results[1]['summary-graph-type'].iloc[0]
                                      (**residential_results[1]['parameters'].iloc[0],
                                       name=summary_results[1]['residential-trace-name'].iloc[0]))
            for j in range(len(building_index[i])):
                building_fig.add_trace(residential_results[1]['graph-type'].iloc[building_index[i][j]]
                                       (**residential_results[1]['parameters'].iloc[building_index[i][j]],
                                        name=residential_results[1]['line-name'].iloc[building_index[i][j]]))
            building_fig.update_layout(
                title=residential_results[1]['graph-title'].iloc[0],
                xaxis_title=residential_results[1]['x-label'].iloc[0],
                yaxis_title=residential_results[1]['y-label'].iloc[0],
                xaxis_tickformat='%Y',
                xaxis=dict(
                    rangeslider=dict(
                        visible=True
                    ),
                    type="date"
                ),
            )
    summary_fig.update_layout(
        barmode='stack',
        title=summary_results[1]['graph-title'].iloc[0],
        xaxis_title=summary_results[1]['x-label'].iloc[0],
        yaxis_title=summary_results[1]['y-label'].iloc[0],
        xaxis_tickformat='%Y',
        xaxis=dict(
            rangeslider=dict(
                visible=True
            ),
            type="date"
        ),
    )
    return transportation_fig, building_fig, summary_fig


@app.callback(Output("transit-graph", "figure"), Input('never-used', 'n_clicks'))
def display_pie_chart(never_used):
    """A callback that displays a pie chart for the transit route page.

        :param never_used: A random input that is never used.

        :returns: A plotly_graph_objs pie chart with a breakdown of the composition of the victoria transit bus fleet.
    """
    fig = go.Figure(data=[
        go.Pie(labels=transit_results[1]['chart-label'], values=transit_results[1]['buses'], textinfo='label+percent',
               )])
    return fig


@app.callback(
    dash.dependencies.Output('transit-slider-output-container', 'children'),
    [dash.dependencies.Input('transit-slider', 'value')])
def update_output(value):
    """A callback that displays the output of the year slider for the transit route page.

            :param value: The value of the year slider.

            :returns: A string containing the value of the year slider to be displayed just under the slider.
    """
    return 'Year = {}'.format(value)


@app.callback(
    dash.dependencies.Output('transportation-sub-sectors', 'children'),
    [dash.dependencies.Input('never-used', 'n_clicks')])
def create_sub_sector_menu(never_used):
    """A callback that creates the dropdown menu used by users to navigate thru the transportation sub-sector pages.

        :param never_used: Input that is never used.

        :returns: A dbc dropdown menu for the navigate to all the transportation sub-sector pages.
    """
    return dbc.DropdownMenu(
        id={
            'type': 'transportation-sub-sectors',
            'index': 1
        },
        children=get_sub_sectors(transportation_results),
        label=transportation_results[1]['sub-sector-menu'].iloc[0][0]
    )


if __name__ == '__main__':
    app.run_server()
